dkr_cimiss_retrieve_station <- function(times,
                                        elements="Station_Id_C,Datetime,TEM",
                                        staIds="54511"){
  # Retrieve data by cimiss music API.
  # You should install MUSIC java client.
  #    1) download MUSIC java client from http://10.20.76.55/cimissapiweb/page/apipages/ClientDocumentRight.jsp
  #    2) set environment variable
  #       CLASSPATH=C:\soft\music-lib\Ice.jar;C:\soft\music-lib\music-client-v1.4.0.jar;C:\soft\music-lib
  #    3) call java function
  #
  # Args:
  #   times: times for retrieve, 'YYYYMMDDHHMISS,YYYYMMDDHHMISS,...'.
  #   elements: elements for retrieve, 'ele1,ele2,...'.
  #   staIds: station ids, 'xxxxx,xxxxx,...'.
  #
  # Returns:
  #   data frame of observations.
  
  # load and init packages
  require(rJava)
  .jinit()

  # retrieve parameters
  params = .jnew("java/util/HashMap")
  .jrcall(params, "put", "dataCode","SURF_CHN_MUL_HOR_N")
  .jrcall(params, "put", "elements",elements)
  .jrcall(params, "put", "times",times)
  .jrcall(params, "put", "staIds",staIds)
  .jrcall(params, "put", "orderby","Datetime:ASC")
  userId = "NMC_YBS_daikan"
  pwd = "DaiKan1998"
  interfaceId = "getSurfEleByTimeAndStaID"
  
  # retrieve data
  retArray2D = .jnew("cma/cimiss/RetArray2D")
  client = .jnew("cma/cimiss/client/DataQueryClient")
  .jcall(client,"V","initResources")
  rst = .jcall(client, "I", "callAPI_to_array2D", userId, pwd, interfaceId, params, retArray2D)
  .jcall(client,"V","destroyResources")
  
  # extract data
  data = as.data.frame(retArray2D$data,stringsAsFactors=FALSE)
  colnames(data) <- unlist(strsplit(elements,","))
  data$TEM = as.numeric(data$TEM)
  data[data == 999999.0] <- NA
  
  # return data
  return(data)
}
